package main

import (
	"context"
	"fmt"
	"io"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/client/entities/gallery"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceGalleryClient(conn)

	requestFindAllGallery := pb.RequestFindAllGallery{
		Gallery: &pb.SearchGallery{},
		Pagination: &pb.RequestPagination{
			Limit:      100,
			PageNumber: 1,
			Offset:     0,
		},
		Key: "image",
	}

	requestFindAllGallery.Gallery.Image = append(requestFindAllGallery.Gallery.Image, "update")
	requestFindAllGallery.Gallery.Image = append(requestFindAllGallery.Gallery.Image, "anone")

	stream, err := c.FindAll(context.Background(), &requestFindAllGallery)

	if err != nil {
		log.Fatalf("Error when finding gallery : %s", err)
	} else {
		var responsePagination pb.ResponsePagination
		i := 0
		fmt.Println("Data from stream: ")
		for {
			dataFromServer, err := stream.Recv()
			if i == 0 {
				responsePagination.TotalPage = dataFromServer.Pagination.TotalPage
				responsePagination.TotalItems = dataFromServer.Pagination.TotalItems
				responsePagination.Limit = dataFromServer.Pagination.Limit
				responsePagination.PageNumber = dataFromServer.Pagination.PageNumber
			}

			if err == io.EOF {
				break
			}

			if err != nil {
				log.Println("Error", err)
				break
			}

			fmt.Printf("%v \n", dataFromServer.Gallery[i])
			i += 1
		}
		fmt.Println("Pagination")
		fmt.Printf("Total Page: %d, Total Items: %d, Limit: %d, Page Number: %d",
			responsePagination.TotalPage,
			responsePagination.TotalItems,
			responsePagination.Limit,
			responsePagination.PageNumber)
	}
}
