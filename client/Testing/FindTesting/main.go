package main

import (
	"context"
	"fmt"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/client/entities/gallery"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceGalleryClient(conn)

	/*
		dummy data

		timestamp, test := ptypes.TimestampProto(time.Now())

		if test != nil {
			fmt.Print(test)
		}

		response, err := c.Add(context.Background(), &pb.RequestProcessGallery{
			Process: &pb.Process{
				Id:            "e170be74-8b37-11e8-9eb6-529269fb1459",
				Name:          "Process B Create",
				Origin:        "Coordinator B",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "e170c2c0-8b37-11e8-9eb6-529269fb1459",
					Name:       pb.SubProcessName_CREATE_GALLERY,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Gallery: &pb.Gallery{
				Id:           1,
				Image:        "anone anone",
				ProfileImage: "platella",
				StatusId:     1,
				CreatedBy:    1,
				CreatedAt:    timestamp,
				UpdatedAt:    timestamp,
				Version:      1,
			},
		})

		if err != nil {
			log.Fatalf("Error when calling add : %s", err)
		}

		fmt.Printf("\n Response from server: \n Process=%s \n gallery=%s \n\n\n", response.GetProcess(), response.GetGallery())
	*/

	requestFindOneGallery := pb.RequestFindOneGallery{
		Gallery: &pb.SearchGallery{},
		Key:     "image",
	}

	requestFindOneGallery.Gallery.Image = append(requestFindOneGallery.Gallery.Image, "anone")

	request, err := c.FindOne(context.Background(), &requestFindOneGallery)

	if err != nil {
		log.Fatalf("Error when finding gallery : %s", err)
	} else {
		log.Infof("Get data from key name")
		fmt.Printf("\n %s", request.GetGallery())
	}
}
