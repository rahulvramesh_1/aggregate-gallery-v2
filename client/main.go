package main

import (
	"fmt"
	"strconv"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/client/entities/gallery"
	ptypes "github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn               *grpc.ClientConn
		responseTimeCreate int64
		responseTimeUpdate int64
	)

	totalLoop := int64(30)
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceGalleryClient(conn)

	ts := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("\nsent at : %d", ts)

	timestamp, test := ptypes.TimestampProto(time.Now())

	if test != nil {
		fmt.Print(test)
	}

	response, err := c.Add(context.Background(), &pb.RequestProcessGallery{
		Process: &pb.Process{
			Id:            "410ca0fe-8b34-11e8-9eb6-529269fb1459",
			Name:          "Process A Create",
			Origin:        "Coordinator A",
			Timestamp:     timestamp,
			TotalSequence: 5,
			UserId:        1,
			SubProcess: &pb.SubProcess{
				Id:         "410ca662-8b34-11e8-9eb6-529269fb1459",
				Name:       pb.SubProcessName_CREATE_GALLERY,
				Timestamp:  timestamp,
				SequenceOf: 1,
			},
		},
		Gallery: &pb.Gallery{
			Id:           1,
			Image:        "image",
			ProfileImage: "profile image",
			StatusId:     1,
			CreatedBy:    1,
			CreatedAt:    timestamp,
			UpdatedAt:    timestamp,
			Version:      1,
		},
	})

	if err != nil {
		log.Fatalf("Error when calling add : %s", err)
	}

	receivedAt := time.Now().UnixNano() / int64(time.Millisecond)
	responseTime := (receivedAt - ts)
	responseTimeCreate += responseTime
	fmt.Printf("\nreceived at : %d, response time : %d", receivedAt, responseTime)
	fmt.Printf("\n Response from server: \n Process=%s \n gallery=%s \n\n\n", response.GetProcess(), response.GetGallery())

	for i := int64(0); i < totalLoop; i++ {
		ts = time.Now().UnixNano() / int64(time.Millisecond)
		fmt.Printf("\nsent at : %d", ts)
		timestamp, _ = ptypes.TimestampProto(time.Now())
		response, err = c.Update(context.Background(), &pb.RequestProcessGallery{
			Process: &pb.Process{
				Id:            "410ca0fe-8b34-11e8-9eb6-529269fb1459",
				Name:          "Process A Update",
				Origin:        "Coordinator A",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "410ca662-8b34-11e8-9eb6-529269fb1459",
					Name:       pb.SubProcessName_UPDATE_GALLERY,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Gallery: &pb.Gallery{
				Id:           response.GetGallery().Id,
				Image:        "update image" + strconv.Itoa(int(i)),
				ProfileImage: "update profile image" + strconv.Itoa(int(i)),
				StatusId:     response.GetGallery().StatusId + i,
				CreatedBy:    response.GetGallery().CreatedBy,
				CreatedAt:    response.GetGallery().CreatedAt,
				UpdatedAt:    timestamp,
				Version:      response.GetGallery().Version,
			},
		})
		if err != nil {
			log.Fatalf("Error when calling update : %s", err)
		}

		receivedAt = time.Now().UnixNano() / int64(time.Millisecond)
		responseTime = (receivedAt - ts)
		responseTimeUpdate += responseTime
		fmt.Printf("\nreceived at : %d response time : %d", receivedAt, responseTime)
		fmt.Printf("\n Response from server: \n Process=%s \n gallery=%s", response.GetProcess(), response.GetGallery())
	}

	fmt.Printf("average response time create : %d", responseTimeCreate/totalLoop)
	fmt.Printf("average tme update : %d", responseTimeUpdate/totalLoop)
}
