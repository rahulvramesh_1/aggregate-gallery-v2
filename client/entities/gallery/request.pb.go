// Code generated by protoc-gen-go. DO NOT EDIT.
// source: request.proto

package gallery

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "github.com/golang/protobuf/ptypes/timestamp"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type SearchGallery struct {
	Id                   []int64  `protobuf:"varint,1,rep,packed,name=id,proto3" json:"id,omitempty"`
	Image                []string `protobuf:"bytes,2,rep,name=image,proto3" json:"image,omitempty"`
	ProfileImage         []string `protobuf:"bytes,3,rep,name=profile_image,json=profileImage,proto3" json:"profile_image,omitempty"`
	StatusId             []int64  `protobuf:"varint,4,rep,packed,name=status_id,json=statusId,proto3" json:"status_id,omitempty"`
	CreatedBy            []int64  `protobuf:"varint,5,rep,packed,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SearchGallery) Reset()         { *m = SearchGallery{} }
func (m *SearchGallery) String() string { return proto.CompactTextString(m) }
func (*SearchGallery) ProtoMessage()    {}
func (*SearchGallery) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{0}
}
func (m *SearchGallery) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SearchGallery.Unmarshal(m, b)
}
func (m *SearchGallery) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SearchGallery.Marshal(b, m, deterministic)
}
func (dst *SearchGallery) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SearchGallery.Merge(dst, src)
}
func (m *SearchGallery) XXX_Size() int {
	return xxx_messageInfo_SearchGallery.Size(m)
}
func (m *SearchGallery) XXX_DiscardUnknown() {
	xxx_messageInfo_SearchGallery.DiscardUnknown(m)
}

var xxx_messageInfo_SearchGallery proto.InternalMessageInfo

func (m *SearchGallery) GetId() []int64 {
	if m != nil {
		return m.Id
	}
	return nil
}

func (m *SearchGallery) GetImage() []string {
	if m != nil {
		return m.Image
	}
	return nil
}

func (m *SearchGallery) GetProfileImage() []string {
	if m != nil {
		return m.ProfileImage
	}
	return nil
}

func (m *SearchGallery) GetStatusId() []int64 {
	if m != nil {
		return m.StatusId
	}
	return nil
}

func (m *SearchGallery) GetCreatedBy() []int64 {
	if m != nil {
		return m.CreatedBy
	}
	return nil
}

type RequestPagination struct {
	Limit                int32    `protobuf:"varint,1,opt,name=limit,proto3" json:"limit,omitempty"`
	PageNumber           int32    `protobuf:"varint,2,opt,name=page_number,json=pageNumber,proto3" json:"page_number,omitempty"`
	Offset               int32    `protobuf:"varint,3,opt,name=offset,proto3" json:"offset,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RequestPagination) Reset()         { *m = RequestPagination{} }
func (m *RequestPagination) String() string { return proto.CompactTextString(m) }
func (*RequestPagination) ProtoMessage()    {}
func (*RequestPagination) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{1}
}
func (m *RequestPagination) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RequestPagination.Unmarshal(m, b)
}
func (m *RequestPagination) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RequestPagination.Marshal(b, m, deterministic)
}
func (dst *RequestPagination) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RequestPagination.Merge(dst, src)
}
func (m *RequestPagination) XXX_Size() int {
	return xxx_messageInfo_RequestPagination.Size(m)
}
func (m *RequestPagination) XXX_DiscardUnknown() {
	xxx_messageInfo_RequestPagination.DiscardUnknown(m)
}

var xxx_messageInfo_RequestPagination proto.InternalMessageInfo

func (m *RequestPagination) GetLimit() int32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

func (m *RequestPagination) GetPageNumber() int32 {
	if m != nil {
		return m.PageNumber
	}
	return 0
}

func (m *RequestPagination) GetOffset() int32 {
	if m != nil {
		return m.Offset
	}
	return 0
}

type RequestFindAllGallery struct {
	Gallery              *SearchGallery     `protobuf:"bytes,1,opt,name=gallery,proto3" json:"gallery,omitempty"`
	Pagination           *RequestPagination `protobuf:"bytes,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
	Key                  string             `protobuf:"bytes,3,opt,name=key,proto3" json:"key,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *RequestFindAllGallery) Reset()         { *m = RequestFindAllGallery{} }
func (m *RequestFindAllGallery) String() string { return proto.CompactTextString(m) }
func (*RequestFindAllGallery) ProtoMessage()    {}
func (*RequestFindAllGallery) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{2}
}
func (m *RequestFindAllGallery) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RequestFindAllGallery.Unmarshal(m, b)
}
func (m *RequestFindAllGallery) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RequestFindAllGallery.Marshal(b, m, deterministic)
}
func (dst *RequestFindAllGallery) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RequestFindAllGallery.Merge(dst, src)
}
func (m *RequestFindAllGallery) XXX_Size() int {
	return xxx_messageInfo_RequestFindAllGallery.Size(m)
}
func (m *RequestFindAllGallery) XXX_DiscardUnknown() {
	xxx_messageInfo_RequestFindAllGallery.DiscardUnknown(m)
}

var xxx_messageInfo_RequestFindAllGallery proto.InternalMessageInfo

func (m *RequestFindAllGallery) GetGallery() *SearchGallery {
	if m != nil {
		return m.Gallery
	}
	return nil
}

func (m *RequestFindAllGallery) GetPagination() *RequestPagination {
	if m != nil {
		return m.Pagination
	}
	return nil
}

func (m *RequestFindAllGallery) GetKey() string {
	if m != nil {
		return m.Key
	}
	return ""
}

type RequestFindOneGallery struct {
	Gallery              *SearchGallery `protobuf:"bytes,1,opt,name=gallery,proto3" json:"gallery,omitempty"`
	Key                  string         `protobuf:"bytes,2,opt,name=key,proto3" json:"key,omitempty"`
	XXX_NoUnkeyedLiteral struct{}       `json:"-"`
	XXX_unrecognized     []byte         `json:"-"`
	XXX_sizecache        int32          `json:"-"`
}

func (m *RequestFindOneGallery) Reset()         { *m = RequestFindOneGallery{} }
func (m *RequestFindOneGallery) String() string { return proto.CompactTextString(m) }
func (*RequestFindOneGallery) ProtoMessage()    {}
func (*RequestFindOneGallery) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{3}
}
func (m *RequestFindOneGallery) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RequestFindOneGallery.Unmarshal(m, b)
}
func (m *RequestFindOneGallery) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RequestFindOneGallery.Marshal(b, m, deterministic)
}
func (dst *RequestFindOneGallery) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RequestFindOneGallery.Merge(dst, src)
}
func (m *RequestFindOneGallery) XXX_Size() int {
	return xxx_messageInfo_RequestFindOneGallery.Size(m)
}
func (m *RequestFindOneGallery) XXX_DiscardUnknown() {
	xxx_messageInfo_RequestFindOneGallery.DiscardUnknown(m)
}

var xxx_messageInfo_RequestFindOneGallery proto.InternalMessageInfo

func (m *RequestFindOneGallery) GetGallery() *SearchGallery {
	if m != nil {
		return m.Gallery
	}
	return nil
}

func (m *RequestFindOneGallery) GetKey() string {
	if m != nil {
		return m.Key
	}
	return ""
}

type RequestProcessGallery struct {
	Process              *Process `protobuf:"bytes,1,opt,name=process,proto3" json:"process,omitempty"`
	Gallery              *Gallery `protobuf:"bytes,2,opt,name=gallery,proto3" json:"gallery,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RequestProcessGallery) Reset()         { *m = RequestProcessGallery{} }
func (m *RequestProcessGallery) String() string { return proto.CompactTextString(m) }
func (*RequestProcessGallery) ProtoMessage()    {}
func (*RequestProcessGallery) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{4}
}
func (m *RequestProcessGallery) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RequestProcessGallery.Unmarshal(m, b)
}
func (m *RequestProcessGallery) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RequestProcessGallery.Marshal(b, m, deterministic)
}
func (dst *RequestProcessGallery) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RequestProcessGallery.Merge(dst, src)
}
func (m *RequestProcessGallery) XXX_Size() int {
	return xxx_messageInfo_RequestProcessGallery.Size(m)
}
func (m *RequestProcessGallery) XXX_DiscardUnknown() {
	xxx_messageInfo_RequestProcessGallery.DiscardUnknown(m)
}

var xxx_messageInfo_RequestProcessGallery proto.InternalMessageInfo

func (m *RequestProcessGallery) GetProcess() *Process {
	if m != nil {
		return m.Process
	}
	return nil
}

func (m *RequestProcessGallery) GetGallery() *Gallery {
	if m != nil {
		return m.Gallery
	}
	return nil
}

type RequestRollbackGallery struct {
	Process              *Process `protobuf:"bytes,1,opt,name=process,proto3" json:"process,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RequestRollbackGallery) Reset()         { *m = RequestRollbackGallery{} }
func (m *RequestRollbackGallery) String() string { return proto.CompactTextString(m) }
func (*RequestRollbackGallery) ProtoMessage()    {}
func (*RequestRollbackGallery) Descriptor() ([]byte, []int) {
	return fileDescriptor_request_3af469f7e338ffbe, []int{5}
}
func (m *RequestRollbackGallery) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RequestRollbackGallery.Unmarshal(m, b)
}
func (m *RequestRollbackGallery) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RequestRollbackGallery.Marshal(b, m, deterministic)
}
func (dst *RequestRollbackGallery) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RequestRollbackGallery.Merge(dst, src)
}
func (m *RequestRollbackGallery) XXX_Size() int {
	return xxx_messageInfo_RequestRollbackGallery.Size(m)
}
func (m *RequestRollbackGallery) XXX_DiscardUnknown() {
	xxx_messageInfo_RequestRollbackGallery.DiscardUnknown(m)
}

var xxx_messageInfo_RequestRollbackGallery proto.InternalMessageInfo

func (m *RequestRollbackGallery) GetProcess() *Process {
	if m != nil {
		return m.Process
	}
	return nil
}

func init() {
	proto.RegisterType((*SearchGallery)(nil), "gallery.SearchGallery")
	proto.RegisterType((*RequestPagination)(nil), "gallery.RequestPagination")
	proto.RegisterType((*RequestFindAllGallery)(nil), "gallery.RequestFindAllGallery")
	proto.RegisterType((*RequestFindOneGallery)(nil), "gallery.RequestFindOneGallery")
	proto.RegisterType((*RequestProcessGallery)(nil), "gallery.RequestProcessGallery")
	proto.RegisterType((*RequestRollbackGallery)(nil), "gallery.RequestRollbackGallery")
}

func init() { proto.RegisterFile("request.proto", fileDescriptor_request_3af469f7e338ffbe) }

var fileDescriptor_request_3af469f7e338ffbe = []byte{
	// 389 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x52, 0xc1, 0xae, 0x93, 0x40,
	0x14, 0x0d, 0x60, 0xdf, 0x93, 0x5b, 0x31, 0xcf, 0x89, 0x36, 0xa4, 0xc6, 0xbc, 0x06, 0x37, 0x8d,
	0x8b, 0xd6, 0xd4, 0x9d, 0x3b, 0x8d, 0xd1, 0x74, 0xa3, 0xcd, 0xb8, 0x74, 0x41, 0x06, 0xb8, 0xe0,
	0xa4, 0x03, 0x83, 0xcc, 0xb0, 0xe0, 0x47, 0xf4, 0x77, 0x0d, 0x33, 0x43, 0xb5, 0x7d, 0xbb, 0xee,
	0x38, 0xe7, 0x1e, 0xce, 0x39, 0x73, 0x73, 0x21, 0xea, 0xf0, 0x57, 0x8f, 0x4a, 0x6f, 0xda, 0x4e,
	0x6a, 0x49, 0x6e, 0x2b, 0x26, 0x04, 0x76, 0xc3, 0x32, 0xaa, 0x51, 0x29, 0x56, 0xa1, 0xe5, 0x97,
	0x51, 0xdb, 0xc9, 0x1c, 0x95, 0x72, 0xf0, 0xbe, 0x92, 0xb2, 0x12, 0xb8, 0x35, 0x28, 0xeb, 0xcb,
	0xad, 0xe6, 0x35, 0x2a, 0xcd, 0xea, 0xd6, 0x0a, 0x92, 0xdf, 0x1e, 0x44, 0xdf, 0x91, 0x75, 0xf9,
	0xcf, 0x2f, 0xd6, 0x90, 0x3c, 0x05, 0x9f, 0x17, 0xb1, 0xb7, 0x0a, 0xd6, 0x01, 0xf5, 0x79, 0x41,
	0x9e, 0xc3, 0x8c, 0xd7, 0xac, 0xc2, 0xd8, 0x5f, 0x05, 0xeb, 0x90, 0x5a, 0x40, 0x5e, 0xc3, 0x98,
	0x54, 0x72, 0x81, 0xa9, 0x9d, 0x06, 0x66, 0xfa, 0xc4, 0x91, 0x7b, 0x23, 0x7a, 0x09, 0xa1, 0xd2,
	0x4c, 0xf7, 0x2a, 0xe5, 0x45, 0xfc, 0xc8, 0x38, 0x3e, 0xb6, 0xc4, 0xbe, 0x20, 0xaf, 0x00, 0xf2,
	0x0e, 0x99, 0xc6, 0x22, 0xcd, 0x86, 0x78, 0x66, 0xa6, 0xa1, 0x63, 0x3e, 0x0e, 0x49, 0x06, 0xcf,
	0xa8, 0x7d, 0xf1, 0x81, 0x55, 0xbc, 0x61, 0x9a, 0xcb, 0x66, 0xec, 0x22, 0x78, 0xcd, 0x75, 0xec,
	0xad, 0xbc, 0xf5, 0x8c, 0x5a, 0x40, 0xee, 0x61, 0xde, 0xb2, 0x0a, 0xd3, 0xa6, 0xaf, 0x33, 0xec,
	0x62, 0xdf, 0xcc, 0x60, 0xa4, 0xbe, 0x1a, 0x86, 0x2c, 0xe0, 0x46, 0x96, 0xa5, 0x42, 0x1d, 0x07,
	0x66, 0xe6, 0x50, 0xf2, 0xc7, 0x83, 0x17, 0x2e, 0xe4, 0x33, 0x6f, 0x8a, 0x0f, 0x42, 0x4c, 0x4b,
	0x78, 0x0b, 0xd3, 0x82, 0x4d, 0xd4, 0x7c, 0xb7, 0xd8, 0x38, 0xbc, 0x39, 0xdb, 0x16, 0x9d, 0x64,
	0xe4, 0x3d, 0x8c, 0x89, 0xae, 0xa8, 0xe9, 0x30, 0xdf, 0x2d, 0x4f, 0x3f, 0x3d, 0x78, 0x0a, 0xfd,
	0x4f, 0x4d, 0xee, 0x20, 0x38, 0xe2, 0x60, 0xca, 0x85, 0x74, 0xfc, 0x4c, 0x7e, 0x9c, 0x15, 0xfb,
	0xd6, 0xe0, 0xf5, 0xc5, 0x9c, 0xb9, 0xff, 0xcf, 0x5c, 0x9e, 0xcc, 0x0f, 0xf6, 0x58, 0x26, 0xf3,
	0x37, 0x70, 0xeb, 0xce, 0xc7, 0x99, 0xdf, 0x9d, 0xcc, 0x9d, 0x92, 0x4e, 0x82, 0x51, 0x3b, 0x15,
	0xf1, 0x2f, 0xb4, 0x97, 0x15, 0x92, 0x4f, 0xb0, 0x70, 0x81, 0x54, 0x0a, 0x91, 0xb1, 0xfc, 0x78,
	0x45, 0x62, 0x76, 0x63, 0x2e, 0xf6, 0xdd, 0xdf, 0x00, 0x00, 0x00, 0xff, 0xff, 0x39, 0x84, 0xbb,
	0x3c, 0x0a, 0x03, 0x00, 0x00,
}
