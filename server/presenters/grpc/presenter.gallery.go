package grpc_presenter

import (
	"encoding/json"
	"errors"
	"math"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/gallery"
	"bitbucket.org/evhivetech/aggregate-gallery-v2/server/usecases"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type GalleryGRPCPresenter struct {
	usecase usecases.UsecaseGallery
}

func NewGalleryGRPCPresenter(sv *grpc.Server, uc usecases.UsecaseGallery) {
	p := &GalleryGRPCPresenter{
		usecase: uc,
	}

	pb.RegisterServiceGalleryServer(sv, p)
}

func (p *GalleryGRPCPresenter) FindAll(ctx *pb.RequestFindAllGallery, stream pb.ServiceGallery_FindAllServer) error {
	var (
		err      error
		response *pb.ResponseFindAllGallery
	)

	response = &pb.ResponseFindAllGallery{
		Pagination: &pb.ResponsePagination{
			Limit: ctx.Pagination.Limit,
		},
	}

	if ctx.Pagination.PageNumber == 0 {
		response.Pagination.PageNumber = 1
	} else {
		response.Pagination.PageNumber = ctx.Pagination.PageNumber
	}

	response.Gallery, err = p.usecase.FindAll(ctx.GetGallery(), int(ctx.Pagination.Limit), int(ctx.Pagination.Offset), ctx.Key)
	if err != nil {
		log.Error(err)
		return err
	}

	if len(response.Gallery) < 10 {
		response.Pagination.TotalPage = 1
		response.Pagination.TotalItems = int32(len(response.Gallery))
	} else {
		responseGalleryLengthAsFloat64 := float64(len(response.Gallery))
		responseGalleryLengthAsint32 := int32(len(response.Gallery))

		//10 is number of gallery in one page
		response.Pagination.TotalPage = responseGalleryLengthAsint32 / 10
		if math.Mod(responseGalleryLengthAsFloat64, 10) != 0 {
			response.Pagination.TotalPage += 1
		}

		response.Pagination.TotalItems = responseGalleryLengthAsint32
	}

	for i := 0; i < len(response.Gallery); i++ {
		if err := stream.Send(response); err != nil {
			return err
		}
	}

	return err
}

func (p *GalleryGRPCPresenter) FindOne(ctx context.Context, params *pb.RequestFindOneGallery) (*pb.ResponseFindOneGallery, error) {
	var (
		err      error
		response *pb.ResponseFindOneGallery
	)

	response = &pb.ResponseFindOneGallery{
		Gallery: &pb.Gallery{},
	}

	response.Gallery, err = p.usecase.FindOne(params.GetGallery(), params.Key)
	if err != nil {
		log.Error(err)
		return response, err
	}
	return response, err
}

func (p *GalleryGRPCPresenter) Add(ctx context.Context, params *pb.RequestProcessGallery) (*pb.ResponseProcessGallery, error) {
	var (
		err      error
		response *pb.ResponseProcessGallery
	)

	response = &pb.ResponseProcessGallery{
		Process: params.GetProcess(),
		Gallery: params.GetGallery(),
	}

	response.Process, response.Gallery, err = p.usecase.AddOne(params.GetProcess(), params.GetGallery())
	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *GalleryGRPCPresenter) Update(ctx context.Context, params *pb.RequestProcessGallery) (*pb.ResponseProcessGallery, error) {
	var (
		err      error
		response *pb.ResponseProcessGallery
	)

	response = &pb.ResponseProcessGallery{
		Process: params.GetProcess(),
		Gallery: params.GetGallery(),
	}

	response.Process, response.Gallery, err = p.usecase.UpdateOne(params.GetProcess(), params.GetGallery())

	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *GalleryGRPCPresenter) Remove(ctx context.Context, params *pb.RequestProcessGallery) (*pb.ResponseProcessGallery, error) {
	var (
		err      error
		response *pb.ResponseProcessGallery
		proc     *pb.Process
		gallery  *pb.Gallery
	)

	proc, gallery, err = p.usecase.RemoveOne(params.GetProcess(), params.GetGallery())

	response = &pb.ResponseProcessGallery{
		Process: proc,
		Gallery: gallery,
	}

	return response, err
}

func (p *GalleryGRPCPresenter) Rollback(ctx context.Context, params *pb.RequestRollbackGallery) (*pb.ResponseRollbackGallery, error) {
	var (
		err      error
		response *pb.ResponseRollbackGallery
	)

	return response, err
}
