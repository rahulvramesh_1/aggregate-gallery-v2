package usecases

import (
	"encoding/json"
	"errors"
	"math"
	"time"

	es "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/gallery"
	"bitbucket.org/evhivetech/aggregate-gallery-v2/server/repositories"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

type UsecaseGallery interface {
	FindOne(params *pb.SearchGallery, key string) (*pb.Gallery, error)
	FindAll(params *pb.SearchGallery, limit int, offset int, key string) ([]*pb.Gallery, error)
	Count(params *pb.SearchGallery) (int, error)

	AddOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error)
	UpdateOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error)
	RemoveOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error)
}

type RepositoryGallery struct {
	C repository.C
	Q repository.Q
}

func NewGalleryUseCase(c repository.C, q repository.Q) UsecaseGallery {
	return &RepositoryGallery{
		C: c,
		Q: q,
	}
}

func (repo *RepositoryGallery) FindOne(params *pb.SearchGallery, key string) (*pb.Gallery, error) {
	var (
		gallery *pb.Gallery
		err     error
	)

	_params := &pb.Gallery{}

	switch key {
	case "id":
		if len(params.Id) != 0 {
			_params.Id = int64(params.Id[0])
		}
	case "image":
		if len(params.Image) != 0 {
			_params.Image = params.Image[0]
		}
	case "profile_image":
		if len(params.ProfileImage) != 0 {
			_params.ProfileImage = params.ProfileImage[0]
		}
	case "status_id":
		if len(params.StatusId) != 0 {
			_params.StatusId = int64(params.StatusId[0])
		}
	case "created_by":
		if len(params.CreatedBy) != 0 {
			_params.CreatedBy = int64(params.CreatedBy[0])
		}
	case "default":
		err = errors.New("Wrong key")
	}
	if err != nil {
		log.Error(err)
		return gallery, err
	}

	gallery, err = repo.Q.FindOne(_params, key)
	if err != nil {
		log.Error(err)
		return gallery, err
	}

	return gallery, err
}

func (repo *RepositoryGallery) FindAll(params *pb.SearchGallery, limit int, offset int, key string) ([]*pb.Gallery, error) {
	var (
		gallery []*pb.Gallery
		err     error
	)

	gallery, err = repo.Q.FindAll(params, limit, offset, key)
	if err != nil {
		log.Error(err)
		return gallery, err
	}

	return gallery, err
}

func (repo *RepositoryGallery) Count(params *pb.SearchGallery) (int, error) {
	var (
		totalItems int
		err        error
	)

	return totalItems, err
}

func (repo *RepositoryGallery) AddOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error) {
	var (
		err    error
		procTs time.Time
	)

	//build events from gallery
	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	prev := &pb.Gallery{}
	eventSource := es.EventSource{
		ID:        0,
		Name:      es.ENTITY_NAME,
		Version:   0,
		CreatedAt: procTs,
		UpdatedAt: time.Time{},
	}

	eventStreams, err := es.BuildEventsFromGallery(proc, prev, gallery)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.WriteNewStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//write snapshot
	tempGallery := &pb.Gallery{}
	gallery, err = es.BuildGalleryFromEvents(eventSource, eventStreams, tempGallery)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	data, _ := json.Marshal(&gallery)
	ss := &es.Snapshot{
		EventSourceName: eventSource.Name,
		EventSourceID:   eventSource.ID,
		Version:         eventSource.Version,
		Data:            string(data),
		CreatedAt:       eventSource.CreatedAt,
	}
	gallery, err = repo.C.WriteSnapshot(ss)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//create projection on query side
	gallery, err = repo.Q.AddOne(gallery)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	return proc, gallery, err
}

func (repo *RepositoryGallery) UpdateOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error) {
	var (
		err    error
		procTs time.Time
	)

	//sleep is to ensure the data in elasticsearch is the latest
	time.Sleep(1 * time.Second)

	//get version from gallery
	prev, err := repo.Q.FindOne(gallery, "id")
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	createdAt, err := ptypes.Timestamp(prev.CreatedAt)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//check if this version does not match with current version
	if prev.Version != gallery.Version {
		err = errors.New("concurrency exception : version does not match with query layer version")
		log.Error(err)
		return proc, gallery, err
	}

	//fill in created at from previous record and updated at using process timestamp
	eventSource := es.EventSource{
		ID:        gallery.Id,
		Name:      es.ENTITY_NAME,
		Version:   gallery.Version,
		CreatedAt: createdAt,
		UpdatedAt: procTs,
	}

	eventStreams, err := es.BuildEventsFromGallery(proc, prev, gallery)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.AppendToStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	//build gallery
	gallery, err = es.BuildGalleryFromEvents(eventSource, eventStreams, prev)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	if math.Mod(float64(eventSource.Version), 5) == 0 {
		data, _ := json.Marshal(&gallery)
		ss := &es.Snapshot{
			EventSourceName: eventSource.Name,
			EventSourceID:   eventSource.ID,
			Version:         eventSource.Version,
			Data:            string(data),
			CreatedAt:       eventSource.CreatedAt,
		}
		gallery, err = repo.C.WriteSnapshot(ss)
		if err != nil {
			log.Error(err)
			return proc, gallery, err
		}
	}

	//create projection on query side
	gallery, err = repo.Q.UpdateOne(gallery)
	if err != nil {
		log.Error(err)
		return proc, gallery, err
	}

	return proc, gallery, err
}

func (repo *RepositoryGallery) RemoveOne(proc *pb.Process, gallery *pb.Gallery) (*pb.Process, *pb.Gallery, error) {
	var (
		err error
	)
	return proc, gallery, err
}
