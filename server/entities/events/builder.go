package es

import (
	"time"

	jsoniter "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/gallery"
	"github.com/golang/protobuf/ptypes"
)

func CopyEvent(e Event) Event {
	return Event{
		ID:               e.ID,
		ProcessID:        e.ProcessID,
		ProcessCreatedAt: e.ProcessCreatedAt,
		SubProcessID:     e.SubProcessID,
		EventSourceID:    e.EventSourceID,
		EventSourceName:  e.EventSourceName,
		Version:          e.Version,
		Sequence:         e.Sequence,
		Type:             e.Type,
		Data:             string(e.Data),
		CreatedAt:        e.CreatedAt,
	}
}

func BuildEventsFromGallery(process *pb.Process, prev *pb.Gallery, curr *pb.Gallery) ([]Event, error) {
	var events []Event
	var data []byte

	procTS, err := ptypes.Timestamp(process.GetTimestamp())
	if err != nil {
		return events, err
	}

	event := Event{
		ID:               0,
		ProcessID:        process.GetId(),
		ProcessCreatedAt: procTS,
		SubProcessID:     process.GetSubProcess().GetId(),
		EventSourceID:    prev.GetId(),
		EventSourceName:  ENTITY_NAME,
		Version:          prev.GetVersion(),
		Sequence:         0,
		Type:             "",
		Data:             string(data),
		CreatedAt:        time.Now(),
	}

	if prev.Version > 0 && prev.Id > 0 && process.SubProcess.Name == pb.SubProcessName_UPDATE_GALLERY {
		if prev.Image != curr.Image {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = IMAGE_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.ProfileImage != curr.ProfileImage {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = PROFILE_IMAGE_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.StatusId != curr.StatusId {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = STATUS_ID_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}
	} else {
		data, _ = jsoniter.Marshal(&curr)
		event.Type = GALLERY_ADDED
		event.Data = string(data)
		event.Sequence++
		events = append(events, CopyEvent(event))
	}
	return events, err
}

func BuildGalleryFromEvents(es EventSource, events []Event, ss *pb.Gallery) (*pb.Gallery, error) {
	var (
		err     error
		gallery *pb.Gallery
		temp    *pb.Gallery
	)

	//if snapshot exists
	if ss.Id > 0 {
		gallery = ss
	}

	for _, event := range events {
		jsoniter.Unmarshal([]byte(event.Data), &temp)
		switch eventType := event.Type; eventType {
		case GALLERY_ADDED:
			gallery = temp
		case IMAGE_CHANGED:
			gallery.Image = temp.Image
		case PROFILE_IMAGE_CHANGED:
			gallery.ProfileImage = temp.ProfileImage
		case STATUS_ID_CHANGED:
			gallery.StatusId = temp.StatusId
		}
	}

	//compensate payload
	gallery.Id = es.ID
	gallery.Version = es.Version
	gallery.CreatedAt, _ = ptypes.TimestampProto(es.CreatedAt)
	gallery.UpdatedAt, _ = ptypes.TimestampProto(es.UpdatedAt)

	return gallery, err
}
