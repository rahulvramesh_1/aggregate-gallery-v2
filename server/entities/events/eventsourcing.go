package es

import (
	"time"
)

const ENTITY_NAME = "GALLERY"
const GALLERY_ADDED = "GALLERY_ADDED"
const IMAGE_CHANGED = "IMAGE_CHANGED"
const PROFILE_IMAGE_CHANGED = "PROFILE_IMAGE_CHANGED"
const STATUS_ID_CHANGED = "STATUS_ID_CHANGED"
const CREATED_BY_CHANGED = "CREATED_BY_CHANGED"

type EventSource struct {
	ID        int64
	Name      string
	Version   int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Event struct {
	ID               int64
	ProcessID        string
	ProcessCreatedAt time.Time
	SubProcessID     string
	EventSourceID    int64
	EventSourceName  string
	Version          int64
	Sequence         int32
	Type             string
	Data             string
	CreatedAt        time.Time
}

type Snapshot struct {
	EventSourceName string
	EventSourceID   int64
	Version         int64
	Data            string
	CreatedAt       time.Time
}
