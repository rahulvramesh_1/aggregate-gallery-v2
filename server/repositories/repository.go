package repository

import (
	es "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/gallery"
)

//Using command query segragation
type Q interface {
	FindOne(gallery *pb.Gallery, key string) (*pb.Gallery, error)
	FindAll(gallery *pb.SearchGallery, limit int, offset int, key string) ([]*pb.Gallery, error)
	AddOne(gallery *pb.Gallery) (*pb.Gallery, error)
	UpdateOne(gallery *pb.Gallery) (*pb.Gallery, error)
}

type C interface {
	WriteNewStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	AppendToStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) ([]*es.Event, error)

	WriteSnapshot(snapshot *es.Snapshot) (*pb.Gallery, error)
	ReadSnapshot(snapshot *es.Snapshot) (*pb.Gallery, error)
	ReadLatestSnapshot(eventSource es.EventSource) (*pb.Gallery, error)
}
