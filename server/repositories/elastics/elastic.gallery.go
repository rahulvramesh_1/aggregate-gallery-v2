package elastics

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/olivere/elastic"

	pb "bitbucket.org/evhivetech/aggregate-gallery-v2/server/entities/gallery"
	"bitbucket.org/evhivetech/aggregate-gallery-v2/server/repositories"
	"github.com/labstack/gommon/log"
)

type Q_DB struct {
	db *elastic.Client
}

func GetKeyValue(gallery *pb.Gallery, key string) (string, error) {
	var keyValue string
	var err error

	switch key {
	case "id":
		keyValue = strconv.Itoa(int(gallery.Id))
	case "image":
		keyValue = strings.ToLower(gallery.Image)
	case "profile_image":
		keyValue = strings.ToLower(gallery.ProfileImage)
	case "status_id":
		keyValue = strconv.Itoa(int(gallery.StatusId))
	case "created_by":
		keyValue = strconv.Itoa(int(gallery.CreatedBy))
	case "default":
		err = errors.New("Wrong key")
	}

	return keyValue, err
}

func GetQueryClientDB(db *elastic.Client) repository.Q {
	return &Q_DB{db: db}
}

func (q *Q_DB) AddOne(gallery *pb.Gallery) (*pb.Gallery, error) {
	var err error

	_, err = q.db.Index().
		Index("gallery").
		Type("doc").
		Id(strconv.Itoa(int(gallery.Id))).
		BodyJson(gallery).Refresh("wait_for").
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return gallery, err
	}

	return gallery, err
}

func (q *Q_DB) FindOne(gallery *pb.Gallery, key string) (*pb.Gallery, error) {
	var (
		_gallery *pb.Gallery
		keyValue string
		_err     error
	)

	keyValue, _err = GetKeyValue(gallery, key)
	if _err != nil {
		log.Error(_err)
		return _gallery, _err
	}

	termQuery := elastic.NewTermQuery(key, keyValue)
	result, err := q.db.Search().
		Index("gallery").
		Query(termQuery).
		From(0).Size(1).
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return _gallery, err
	}

	if result.Hits.TotalHits > 0 {
		for _, hit := range result.Hits.Hits {
			err := json.Unmarshal(*hit.Source, &_gallery)
			if err != nil {
				log.Error(err)
				return _gallery, err
			}
		}
	}

	return _gallery, err
}

func (q *Q_DB) FindAll(gallery *pb.SearchGallery, limit int, offset int, key string) ([]*pb.Gallery, error) {
	var (
		keyValues []string
		err       error
	)

	listGallery := []*pb.Gallery{}

	a := 0
	for len(listGallery) < limit {
		_gallery := &pb.Gallery{}

		switch key {
		case "id":
			if len(gallery.Id) != 0 && a < len(gallery.Id) {
				_gallery.Id = int64(gallery.Id[a])
			} else {
				return listGallery, err
			}
		case "image":
			if len(gallery.Image) != 0 && a < len(gallery.Image) {
				_gallery.Image = gallery.Image[a]
			} else {
				return listGallery, err
			}
		case "profile_image":
			if len(gallery.ProfileImage) != 0 && a < len(gallery.ProfileImage) {
				_gallery.ProfileImage = gallery.ProfileImage[a]
			} else {
				return listGallery, err
			}
		case "status_id":
			if len(gallery.StatusId) != 0 && a < len(gallery.StatusId) {
				_gallery.StatusId = int64(gallery.StatusId[a])
			} else {
				return listGallery, err
			}
		case "created_by":
			if len(gallery.CreatedBy) != 0 && a < len(gallery.CreatedBy) {
				_gallery.CreatedBy = int64(gallery.CreatedBy[a])
			} else {
				return listGallery, err
			}
		case "default":
			err := errors.New("Wrong key")
			return listGallery, err
		}

		keyValue, _err := GetKeyValue(_gallery, key)
		if _err != nil {
			log.Error(_err)
			return listGallery, _err
		}

		keyValues = append(keyValues, keyValue)

		termQuery := elastic.NewTermQuery(key, keyValues[a])
		result, err := q.db.Search().
			Index("gallery").
			Query(termQuery).
			From(offset).Size(limit).
			Do(context.Background())

		if err != nil {
			log.Error(err)
			return listGallery, err
		}

		if result.Hits.TotalHits > 0 {
			i := 0
			for _, hit := range result.Hits.Hits {
				tempGallery := &pb.Gallery{}
				listGallery = append(listGallery, tempGallery)
				err := json.Unmarshal(*hit.Source, &listGallery[i])
				if err != nil {
					log.Error(err)
					return listGallery, err
				}
				i += 1
			}
		}
		a += 1
	}

	return listGallery, err
}

func (q *Q_DB) UpdateOne(gallery *pb.Gallery) (*pb.Gallery, error) {
	var err error
	_, err = q.db.Index().
		Index("gallery").
		Type("doc").
		Id(strconv.Itoa(int(gallery.Id))).
		BodyJson(gallery).
		Do(context.Background())
	if err != nil {
		log.Error(err)
		return gallery, err
	}
	return gallery, err
}
